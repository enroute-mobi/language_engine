Rails.application.routes.draw do
  resources :languages, only: [:show, :update]
end
