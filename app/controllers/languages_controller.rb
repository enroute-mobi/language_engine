# frozen_string_literal: true

class LanguagesController < ApplicationController
  skip_before_action :authenticate_user!, raise: false

  def show
    change_language
  end

  def update
    change_language
  end

  private

  def change_language
    self.current_language = params[:id]
    redirect_back fallback_location: root_path
  end
end
